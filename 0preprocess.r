library(openxlsx)

original_data <- read.xlsx("./data/附件一：325个样本数据.xlsx",
                           rowNames = T,
                           rows = c(2, 4 : 400))

quest1.s285 <- read.xlsx("./data/附件三：285号和313号样本原始数据.xlsx",
                         rowNames = T,
                         rows = c(2, 4 : 43), sheet = 5)

quest1.s313 <- read.xlsx("./data/附件三：285号和313号样本原始数据.xlsx",
                         rowNames = T,
                         rows = c(2, 45 : 84), sheet = 5)

quest1.material <- read.xlsx("./data/附件三：285号和313号样本原始数据.xlsx",
                             rowNames = T,
                             sheet = 1)

quest1.production <- read.xlsx("./data/附件三：285号和313号样本原始数据.xlsx",
                               rowNames = T,
                               rows = c(1, 3:4),
                               sheet = 2)

quest1.adsorbent.wait <- read.xlsx("./data/附件三：285号和313号样本原始数据.xlsx",
                                   rowNames = T,
                                   rows = c(1, 3:4),
                                   sheet = 3)

quest1.adsorbent.reproduce <- read.xlsx("./data/附件三：285号和313号样本原始数据.xlsx",
                                        rowNames = T,
                                        rows = c(1, 3:4),
                                        sheet = 4)

action_data <- read.xlsx("./data/附件四：354个操作变量信息.xlsx",
                         rowNames = T)

quest1.fun.filter <- function(df) {
    reduce_fn <- function(dff, colname) {
        value_range <- c(min(original_data[,colname]),
                         max(original_data[,colname]))
        dfc <- df[colname]
        cond <- dfc >= value_range[1] & dfc <= value_range[2]
        if (FALSE %in% cond) {
            message(colname, " is not satisfied.\n", summary(cond))
        }
        dff[cond, ]
    }
    Reduce(
        reduce_fn,
        colnames(df)[! colnames(df) %in% c("时间",
                                           "S-ZORB.PT_7503.DACA",
                                           "S-ZORB.TE_3111.DACA")],
        init = df
    )
}

message("filter 285")
quest1.fun.filter(quest1.s285) -> quest1.s285_filtered
message("filter 313")
quest1.fun.filter(quest1.s313) -> quest1.s313_filtered

quest1.s285_mean <- lapply(quest1.s285_filtered, mean)
quest1.s313_mean <- lapply(quest1.s313_filtered, mean)

write.csv(quest1.s285_filtered, "./data/s285_filtered.csv")
write.csv(quest1.s313_filtered, "./data/s313_filtered.csv")
write.csv(quest1.s285_mean, "./data/s285_mean.csv")
write.csv(quest1.s313_mean, "./data/s313_mean.csv")
