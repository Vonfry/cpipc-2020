import pdb

import numpy as np

import pandas as pd
action_data = pd.read_excel('./data/附件四：354个操作变量信息.xlsx', index_col=0)
feature = pd.read_excel('./data/feature.xlsx', sheet_name='Sheet1')['RON_LOSS_AND_S'].dropna(axis=0, how='any').tolist()
df = pd.read_excel('./data/325.xlsx', index_col=0, sheet_name='Sheet2').drop(['时间'] ,axis=1)

unmodified_feature_index = [5]
unmodified_feature = [ feature[i] for i in unmodified_feature_index ]
modified_feature = [ feature[i] for i in range(0, len(feature)) if not i in unmodified_feature_index]

import re
normalization_record = {}
def get_var_range(index):
    str_range = action_data[action_data['位号'] == index].iloc[0]['取值范围']
    str_range = re.sub(r'\(|\)|（|）', '', str_range)
    str_range = re.sub(r'(?<=-)?(?<=\d)+-', '/', str_range, 1)
    num_range = list(map(float, str_range.split('/')))
    def norm_range(num):
        mean = df[index].mean()
        std = df[index].std()
        global normalization_record
        normalization_record[index] = (mean, std)
        return (num - mean) / std
    num_range = list(map(norm_range , num_range))
    return num_range

from keras.models import load_model
model = load_model('./data/RON_LOSS_AND_S.h5')

df_ron_loss = df['RON损失']
df_ron_loss_norm = (df_ron_loss - df_ron_loss.mean()) / df_ron_loss.std()
norm_ron_loss_zero = (0 - df_ron_loss.mean()) / df_ron_loss.std()
df_s = df['_硫含量']
norm_s = (5 - df_s.mean()) / df_s.std()
norm_s_zero = (0 - df_s.mean()) / df_s.std()
df_std_s = df.std()['_硫含量']
df_mean_s = df.mean()['_硫含量']
df_std_ron_loss = df.std()['RON损失']
df_mean_ron_loss = df.mean()['RON损失']

def merge_feature_args(unmodified, modified):
    for i in range(0, len(unmodified_feature_index)):
        modified.insert(unmodified_feature_index[i], unmodified[i])

def fitness(fix_args, rowindex):
    def fitness_fun(args):
        pred_args = args.tolist()
        merge_feature_args(fix_args, pred_args)
        # step1: calculate target value
        # pdb.set_trace()
        (pred_ron_loss, pred_s) = model.predict(np.array([pred_args]))[0].tolist()
        # step2: check sulfur
        if pred_ron_loss < norm_ron_loss_zero:
            return 2
        if not (pred_s > norm_s_zero and pred_s <= norm_s):
            return 1
        # step3: compute RON LOSS RATE
        ron_loss = df_ron_loss.iloc[rowindex]
        loss_reduce_rate = (ron_loss - (pred_ron_loss * df_std_ron_loss + df_mean_ron_loss) ) / ron_loss
        if loss_reduce_rate > 0.8 :
            return 0
        if loss_reduce_rate < 0.3:
            return 1
        return -loss_reduce_rate
    return fitness_fun

from sko.GA import GA
modified_feature_range = list(map(get_var_range, modified_feature))
calcount = 0
def calculate_minimum(rowindex):
    global calcount
    print('\ncal count: ', calcount)
    row = df.iloc[rowindex]
    fix_args = row[unmodified_feature]
    fix_means = df[unmodified_feature].mean()
    fix_std = df[unmodified_feature].std()
    norm_fix_args = ((fix_args - fix_means) / fix_std).values.tolist()

    ga = GA(func=fitness(norm_fix_args, rowindex),
            size_pop=50, max_iter=800,
            n_dim=len(modified_feature),
            lb=[r[0] for r in modified_feature_range],
            ub=[r[1] for r in modified_feature_range],
            precision=1e-3)
    best_x, best_y = ga.run()
    print('best_x:', best_x, '\n', 'best_y:', best_y)
    original_best_x = [ best_x[i] * normalization_record[modified_feature[i]][1] + normalization_record[modified_feature[i]][0] for i in range(0, len(best_x))]
    print('original x:', original_best_x)
    best_x_dup = best_x.tolist()
    merge_feature_args(norm_fix_args, best_x_dup)
    pred_ron_loss =  model.predict(np.array([ best_x_dup ]))[0][0]
    print('predicted ron loss: ', pred_ron_loss)
    calcount += 1
    return fix_args.tolist() + original_best_x

optimization = list(map(calculate_minimum, range(0, len(df))))

df_original_best_x = pd.DataFrame(optimization, columns = unmodified_feature + modified_feature)

df_original_best_x.index = df.index

df_original_best_x.to_csv('./data/optimization.csv')

# df_original_best_x = pd.read_csv('./data/optimization.csv', index_col = 0)
df_original_best_x_norm = (df_original_best_x[feature] - df[feature].mean()) / df[feature].std()
optimization_ron_loss_and_s  = model.predict(df_original_best_x_norm[feature])
df_optimization_ron_loss_and_s = pd.DataFrame(optimization_ron_loss_and_s, columns = ['ron_loss', 's'])
original_optimization_ron_loss = df_optimization_ron_loss_and_s['ron_loss'] * df_std_ron_loss + df_mean_ron_loss
original_optimization_s = df_optimization_ron_loss_and_s['s'] * df_std_s + df_mean_s
optimization_analysis_ron_loss_rate_mean = ((df_ron_loss - original_optimization_ron_loss) / df_ron_loss).mean()
optimization_analysis = pd.DataFrame({
    'ron_loss_rate_mean': [optimization_analysis_ron_loss_rate_mean],
    's_mean': [original_optimization_s.mean()]
})
optimization_analysis.to_csv('./data/optimization_analysis.csv')

diff = df[feature] - df_original_best_x
diff.to_csv('./data/optimization-diff.csv')

diff_mean = diff.mean()
diff_mean.to_csv('./data/optimization-diff-mean.csv')

diff_norm = (diff - diff_mean) / diff.std()

df_norm = (df - df.mean()) / df.std()

sample_index = 132
df_sample_ron_loss = df.iloc[sample_index]['RON损失']
df_sample_ron_loss_norm = df_norm.iloc[sample_index]['RON损失']
pred_opted_sample = model.predict(np.array([df_original_best_x_norm[feature].iloc[sample_index]]))[0]
optimization_sample_original_s = pred_opted_sample[1] * df_std_s + df_mean_s
optimization_sample_original_ron_loss = pred_opted_sample[0] * df_std_ron_loss + df_mean_ron_loss
optimization_sample_ron_loss_rate = (df_sample_ron_loss - optimization_sample_original_ron_loss) / df_sample_ron_loss
optimization_sample_analysis = pd.DataFrame({
    'optimized_s': [optimization_sample_original_s],
    'optimized_ron_loss': [optimization_sample_original_ron_loss],
    'ron_loss_rate': [optimization_sample_ron_loss_rate]
})
optimization_sample_analysis.to_csv('./data/optimization_sample_analysis.csv')

optimization_and_original_sample = pd.DataFrame(
    np.array([optimization[sample_index], df[unmodified_feature + modified_feature].iloc[sample_index]]),
    columns = unmodified_feature + modified_feature,
    index = ['optimized', 'original']
)
optimization_and_original_sample.to_csv('./data/optimization-sample-both.csv')

import matplotlib.pyplot as plt

fig_diff, axe_diff = plt.subplots()
axe_diff.boxplot(diff_norm.drop(['饱和烃'], axis=1).T,
                 vert=True,
                 patch_artist=True,
                 labels=[ 'D' + str(i+1) for i in range(0, len(modified_feature))])
axe_diff.hlines(0, 0.5, 16.5, colors='C8', zorder=3)
fig_diff.savefig('./output/optimization-diff.jpg')

fig_sample, axe_sample = plt.subplots()
width_sample = 0.35
x = np.arange(1, len(feature) + 1)
rects1 = axe_sample.bar(x - width_sample/2, df_norm[feature].iloc[sample_index].values, width_sample, label='Original')
rects2 = axe_sample.bar(x + width_sample/2, df_original_best_x_norm[feature].iloc[sample_index].values, width_sample, label='Optimized')
axe_sample.set_xticks(x)
axe_sample.set_xticklabels([ 'D' + str(i + 1) for i in range(0, len(feature))])
axe_sample.set_ylabel('norm values')
axe_sample.legend()
fig_sample.tight_layout()
fig_sample.savefig('./output/optimization-sample.jpg')
