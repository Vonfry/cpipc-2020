
from sklearn.model_selection import ShuffleSplit
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor
from scipy import stats
import numpy as np
import pandas as pd
 
inputfile = '325.xlsx'   #excel输入
data = pd.read_excel(inputfile,index='Date', sheet_name='Sheet2')

X = data.drop(['样本编号','时间','_硫含量','_RON','RON损失'],axis=1) 
X = pd.get_dummies(X)

Y1 = data['_RON'] #标签一个，即需要进行预测的值
Y2 = data['RON损失']
Y3 = data[['_硫含量','_RON']]
Y4 = data['_硫含量']
Y5 = data[['_硫含量','RON损失']]

def de_dim(X,Y):
    model = RandomForestRegressor(random_state=10, max_depth=100)
    model.fit(X, Y)

    features = X.columns
    importances = model.feature_importances_

    df = pd.DataFrame({
        'name':features,
        'importances':importances
    })

    df.sort_values(by="importances" , ascending=False,inplace=True)

    df.to_excel('forest.xlsx')

    result0 = df[0:100]['name'].tolist()

    XX = data[result0]

    XX = XX.apply(lambda x: (x - np.min(x)) / (np.max(x) - np.min(x)))
    
    var = XX.var()#获取每一列的方差
    cols = XX.columns
    col = [ ]
    for i in range(0,len(var)):
        if var[i]>=0.05:   # 将阈值设置为0.001
            col.append(cols[i])

    XXX = data[col]
    XXX = XXX.apply(lambda x: (x - np.min(x)) / (np.max(x) - np.min(x)))

    cor2 = XXX.corr(method='spearman')
    result1 = []
    for index_y,item in cor2.iterrows():
        for index_x, row in item.iteritems():
            if abs(row) > 0.6 :
                break
            if index_x not in result1:
                result1.append(index_x)  
    # print(cor2)          
    return result1

f1 = de_dim(X,Y1)
f2 = de_dim(X,Y2)
f3 = de_dim(X,Y3)
f4 = de_dim(X,Y4)
f5 = de_dim(X,Y5)

df1 = pd.DataFrame({'_RON':f1})
df2 = pd.DataFrame({'RON_LOSS':f2})
df3 = pd.DataFrame({'RON_AND_S':f3})
df4 = pd.DataFrame({'_S':f4})
df5 = pd.DataFrame({'RON_LOSS_AND_S':f5})
    


# d1 = pd.DataFrame({'MIX':f1})
d2 = pd.DataFrame({'MIX':f2})
# d3 = pd.DataFrame({'MIX':f3})
d4 = pd.DataFrame({'MIX':f4})

d_mix = pd.concat([d2,d4], axis=0).drop_duplicates().reset_index(drop=True)

df = pd.concat([df1, df2, df3 ,df4 ,df5, d_mix], axis=1)

df.to_excel('feature.xlsx')
print(df)

# a = [x for x in f1 if x in f4]

# print(a)


