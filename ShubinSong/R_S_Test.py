import units as units
import pandas as pd
import tensorflow as tf
import numpy as np
from keras.models import load_model
import h5py
import matplotlib.pyplot as plt 

data_path = '325.xlsx'
feature_path = 'feature.xlsx'
model_path = 'RON_AND_S.h5'
Label_MIX_name = 'RON_LOSS_AND_S'
info_file = '354.xlsx'

info = pd.read_excel(info_file, sheet_name='Sheet1')
data = pd.read_excel(data_path, sheet_name='Sheet2').drop(['样本编号','时间'],axis=1) 
feature = pd.read_excel(feature_path, sheet_name='Sheet1')[Label_MIX_name].dropna(axis=0, how='any').tolist()
model = load_model(model_path)


for f in feature:
    _ron = []
    _s = []
    x_dim = []
    if f != '饱和烃':
        dlta = info['偏差'][info['位号']==f].values
        _max = info['取值范围'][info['位号']==f].values[0].split('~')[1]
        _min = info['取值范围'][info['位号']==f].values[0].split('~')[0]
        uu = info['单位'][info['位号']==f].values
        # print (dlta)
        for i in range(-80,81):            
            temp = data.loc[132][f] 
            temp = temp + i * dlta
            if temp > float(_min) - 2 * dlta and temp < float(_max) + 2 * dlta:
                x_dim.append(temp)
                _data = data.copy()
                _data.loc[132][f] = temp

                mean = _data.mean()  
                std = _data.std()            
                
                _data = (_data - mean)/std #数据归一            
                x_test = _data[feature].values #特征数据
                
                y = model.predict(x_test) #预测结果            
                y_ron = y[:,0]* std['_RON'] + mean['_RON']
                y_s = y[:,1]* std['_硫含量'] + mean['_硫含量']
                ron = pd.DataFrame(y_ron)[0].values[132]
                s = pd.DataFrame(y_s)[0].values[132]
                _ron.append(ron)
                _s.append(s)
            

        fig = plt.figure(f)
        ax1 = fig.add_subplot()
        ax1.plot(x_dim, _ron, 'b')
        ax1.set_ylabel('RON')
        # ax1.set_xlabel('UNIT' + ': ' + str(uu[0]))
        ax1.set_title(f)
        ax1.set_title(f)
        ax1.vlines(data.loc[132][f],85,90,'y',linestyles = 'dashed')
        ax1.vlines(float(_max),85,90,'gray',linestyles = 'dashed')
        ax1.vlines(float(_min),85,90,'gray',linestyles = 'dashed')
        ax2 = ax1.twinx()
        ax2.plot(x_dim, _s,'r')    
        ax2.set_xlim([x_dim[0], x_dim[len(x_dim)-1]])
        ax2.set_xlabel('X')
        print(f)
        fig.savefig('./NO5_pic/' + f + '.png')
        # print([x_dim[0], x_dim[len(x_dim)-1]])
        # print(x_dim)

# plt.show()


# print(data)








