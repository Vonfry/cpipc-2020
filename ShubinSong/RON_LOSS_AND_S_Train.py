import pandas as pd
import tensorflow as tf
from keras.callbacks import TensorBoard
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.optimizers import Adam,SGD
import numpy as np
import os
import math
from sklearn.metrics import r2_score,mean_squared_error
from sklearn.utils import shuffle

ls = os.listdir('./log')
for i in ls:
	c_path=os.path.join('./log',i)
	os.remove(c_path)

inputfile = '325.xlsx'   #excel输入
featurefile = 'feature.xlsx' #excel输出
modelfile = 'RON_LOSS_AND_S.h5' #神经网络权重保存

da = pd.read_excel(inputfile, sheet_name='Sheet2').drop(['样本编号','时间'],axis=1) #pandas以DataFrame的格式读入excel表
data = shuffle(da).reset_index(drop=True)
feature = pd.read_excel(featurefile, sheet_name='Sheet1')['RON_LOSS_AND_S'].dropna(axis=0, how='any').tolist()

label = ['RON损失','_硫含量']
label_str_1 = 'RON损失'
label_str_2 = '_硫含量'

data_train = data.loc[range(0,260)].copy() #标明excel表从第0行到520行是训练集
data_test_0 = data.loc[range(259,324)].copy()

data_mean = data_train.mean()  
data_std = data_train.std() 
data_train = (data_train - data_mean)/data_std #数据标准化
x_train = data_train[feature].values #特征数据
y_train = data_train[label].values #标签数据

test_mean = data_test_0.mean()
test_std = data_test_0.std() 
data_test = (data_test_0 - test_mean)/test_std
x_test = data_test[feature].values

y_test_RON = data_test_0[label[0]].values
y_test_s = data_test_0[label[1]].values

batch_size = 65
epochs = 6000
learning_rate = 0.004

model = Sequential()  #层次模型
model.add(Dense(40,input_dim=len(feature),kernel_initializer='uniform')) #输入层，Dense表示BP层
model.add(Activation('sigmoid'))  #添加激活函数
model.add(Dense(40,input_dim=40,kernel_initializer='uniform')) #输入层，Dense表示BP层
model.add(Activation('sigmoid'))  #添加激活函数
model.add(Dense(40,input_dim=40,kernel_initializer='uniform')) #输入层，Dense表示BP层
model.add(Activation('sigmoid'))  #添加激活函数
model.add(Dense(20,input_dim=40,kernel_initializer='uniform')) #输入层，Dense表示BP层
model.add(Activation('sigmoid'))  #添加激活函数
model.add(Dense(10,input_dim=20,kernel_initializer='uniform')) #输入层，Dense表示BP层
model.add(Activation('sigmoid'))  #添加激活函数
model.add(Dense(2,input_dim=10))  #输出层
adam = Adam(learning_rate=learning_rate)
model.compile(loss='mean_absolute_error', optimizer=adam) #编译模型
model.fit(x_train, y_train, epochs = epochs, batch_size = batch_size,callbacks=[TensorBoard(log_dir='./log')]) #训练模型1000次
model.save(modelfile) #保存模型


y = model.predict(x_test) 
y_ron = y[:,0]* test_std[label_str_1] + test_mean[label_str_1]
y_s = y[:,1]* test_std[label_str_2] + test_mean[label_str_2]


y_test_RON = pd.DataFrame(y_test_RON)[0].tolist()
y_ron = pd.DataFrame(y_ron)[0].tolist()

score_ron = mean_squared_error(y_test_RON,y_ron)
score2_ron = r2_score(y_test_RON,y_ron)

print('RON_LOSS 均方差：' + str(score_ron))
print('RON_LOSS R2：' + str(score2_ron))

y_test_s = pd.DataFrame(y_test_s)[0].tolist()
y_s = pd.DataFrame(y_s)[0].tolist()

score_s = mean_squared_error(y_test_s,y_s)
score2_s = r2_score(y_test_s,y_s)

print('S_均方差：' + str(score_s))
print('S_R2：' + str(score2_s))

data_pre = pd.DataFrame({
    'RON_LOSS': y_test_RON,
    'RON_LOSS_PRE':y_ron})

data_pre_s = pd.DataFrame({
    'S': y_test_s,
    'S_PRED':y_s})

#6 画出预测结果图
import matplotlib.pyplot as plt 
p = data_pre[['RON_LOSS','RON_LOSS_PRE']].plot(style=['b-o','r-*'])
p = data_pre_s[['S','S_PRED']].plot(style=['b-o','r-*'])
plt.show()