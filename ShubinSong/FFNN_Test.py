import units as units
import pandas as pd
import tensorflow as tf
import numpy as np


data_path = '325.xlsx'
feature_path = 'feature.xlsx'
model_path = 'modelweight.model'

data = pd.read_excel(data_path, sheet_name='Sheet2').drop(['样本编号','时间'],axis=1) 
feature = pd.read_excel(feature_path, sheet_name='Sheet1')['name'].tolist()
model = tf.keras.models.load_model(model_path)

mse_ron,r2_ron,mse_ron_dlta,r2_ron_dlta = units.FFNN_test(model,data,feature)

print(mse_ron)
print(r2_ron)
print(mse_ron_dlta)
print(r2_ron_dlta)




