import pandas as pd
import tensorflow as tf
from keras.callbacks import TensorBoard
from keras.models import Sequential
from sklearn.metrics import r2_score,mean_squared_error

def FFNN_test(model,x,feature,label = '_RON'):
    mean = x.mean()  
    std = x.std() 
    x_test = (x - mean)/std #数据归一
    x_test = x[feature].values #特征数据
    y_test = x[label].values    
    y = model.predict(x_test) #预测结果
    y = y * std[label] + mean[label] #数据反归一
    y = pd.DataFrame(y)[0].values

    # mse_ron = mean_squared_error(y_test,y)
    # r2_ron = r2_score(y_test,y)

    RON = x['RON'].values
    RON_LOSS = x['RON损失'].values
    RON_dlta =  RON - y

    # mse_ron_dlta = mean_squared_error(RON_LOSS,RON_dlta)
    # r2_ron_dlta = r2_score(RON_LOSS,RON_dlta)

    return y,RON_dlta