import units as units
import pandas as pd
import tensorflow as tf
import numpy as np
from keras.models import load_model
import h5py

data_path = '325.xlsx'
feature_path = 'feature.xlsx'
model_path = 'RON_LOSS_AND_S.h5'
Label_MIX_name = 'RON_LOSS_AND_S'

data = pd.read_excel(data_path, sheet_name='Sheet2').drop(['样本编号','时间'],axis=1) 
feature = pd.read_excel(feature_path, sheet_name='Sheet1')[Label_MIX_name].dropna(axis=0, how='any').tolist()
model = load_model(model_path)

data_mean = data.mean()  
data_std = data.std() 
data_train = (data - data_mean)/data_std
x_train = data_train[feature].values

y = model.predict(x_train)

y_ron = y[:,0]* data_std['RON损失'] + data_mean['RON损失']
y_s = y[:,1]* data_std['_硫含量'] + data_mean['_硫含量']
    
RON_LOSS = pd.DataFrame(y_ron)[0].values
S = pd.DataFrame(y_s)[0].values

print(RON_LOSS)
print(S)





