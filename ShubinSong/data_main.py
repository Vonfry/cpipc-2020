import pandas as pd
import numpy as np
import math

data_sample = r'325.xlsx'
data_354_path = r'354.xlsx'

data_0 = pd.read_excel(data_sample, sheet_name = 'Sheet1')
data_354 = pd.read_excel(data_354_path, sheet_name = 'Sheet1')

data = data_0.drop(
    ['样本编号','时间','硫含量,μg/g','辛烷值RON','饱和烃,v%（烷烃+环烷烃）','烯烃,v%','芳烃,v%','溴值,gBr/100g','密度(20℃),kg/m³',
    '硫含量μg/g','_辛烷值RON','RON损失（不是变量）','焦炭wt%','Swt%','焦炭,wt%','S, wt%'],
    axis=1)

data_354 = data_354[['位号','取值范围','偏差']]

maxlist = []
minlist = []

for index_y, item in data_354.iterrows():
    maxdata = item['取值范围'].split('~')[1]
    mindata = item['取值范围'].split('~')[0]
    maxlist.append(float(maxdata))
    minlist.append(float(mindata))

ma =  pd.Series(maxlist)     
mi =  pd.Series(minlist) 

data_354['min'] = mi
data_354['max'] = ma

dlta = pd.Series(data_354['偏差'].values, index=data_354['位号'])
during_max = pd.Series(data_354['max'].values, index=data_354['位号']) + dlta
during_min = pd.Series(data_354['min'].values, index=data_354['位号']) - dlta


x = pd.Series()

for index_x, row in data.iteritems():    
    if index_x != '时间':
        j = 0
        s = 0
        for i in row: 
            j=j+1
            s=s+i
            if i == 0:
                j = j -1
        if j == 0:
            x[index_x] = 0
        else:
            x[index_x] = s / j

for index_y, item in data.iterrows():
    for index_x, row in item.iteritems():
        if index_x != '时间':
            if row == 0:                
                data.loc[index_y,index_x]=x[index_x]
                
v_series = pd.Series()

for index_x, row in data.iteritems():
    if index_x != '时间':
        v = []
        for i in row:        
            v.append(x[index_x] - i)

        vv = np.multiply(v,v)
        n = len(v)
        dlta_1 = math.sqrt( (1 / (n - 1) ) * sum(vv) )
        v_series[index_x] = dlta_1 * 3

del_list = []

for index_y, item in data.iterrows():
    for index_x, row in item.iteritems():
        if index_x != '时间':
            if abs(x[index_x] - row) > v_series[index_x] :
                print(index_y,index_x, x[index_x], row ,abs(x[index_x] - row),v_series[index_x])
                if index_y not in del_list:
                    del_list.append(index_y)
            
print(del_list)
data.drop(index = del_list , inplace = True)

del_list_2 = []

for index_y, item in data.iterrows():
    for index_x, row in item.iteritems():
        if index_x != '时间'and index_x != 'S-ZORB.SIS_LT_1001.PV' and index_x != 'S-ZORB.FT_1204.TOTAL' and index_x != 'S-ZORB.AI_2903.PV':      
            if row < during_min[index_x] or row > during_max[index_x] :
                print(index_y, index_x, during_min[index_x],row ,during_max[index_x])
                if index_y not in del_list_2 :
                    del_list_2.append(index_y)

print(del_list_2)
data.drop(index = del_list_2 , inplace = True)

print(data)

# data1 = data.mean()
# data1.to_excel("output1.xlsx",index = True)