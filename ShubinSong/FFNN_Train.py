import pandas as pd
import tensorflow as tf
from keras.callbacks import TensorBoard
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.optimizers import Adam,SGD
import numpy as np
import os
import math
from sklearn.metrics import r2_score,mean_squared_error
from sklearn.utils import shuffle

ls = os.listdir('./log')
for i in ls:
	c_path=os.path.join('./log',i)
	os.remove(c_path)

inputfile = '325.xlsx'   #excel输入
featurefile = 'result.xlsx' #excel输出
modelfile = 'modelweight.model' #神经网络权重保存

da = pd.read_excel(inputfile, sheet_name='Sheet2').drop(['样本编号','时间'],axis=1) #pandas以DataFrame的格式读入excel表
data = shuffle(da).reset_index(drop=True)
feature = pd.read_excel(featurefile, sheet_name='Sheet1')['name'].tolist()

label = ['RON损失'] #标签一个，即需要进行预测的值
# label = ['_RON']
label_str = 'RON损失'  #标签一个，即需要进行预测的值

# print(feature)
data_train = data.loc[range(0,260)].copy() #标明excel表从第0行到520行是训练集
data_test_0 = data.loc[range(259,324)].copy()

data_mean = data_train.mean()  
data_std = data_train.std() 
data_train = (data_train - data_mean)/data_std #数据标准化
x_train = data_train[feature].values #特征数据
y_train = data_train[label].values #标签数据

test_mean = data_test_0.mean()
test_std = data_test_0.std() 
data_test = (data_test_0 - test_mean)/test_std
x_test = data_test[feature].values
y_test = data_test_0[label].values

batch_size = 65
epochs = 4000
learning_rate = 0.005

model = Sequential()  #层次模型
model.add(Dense(5,input_dim=len(feature),kernel_initializer='uniform')) #输入层，Dense表示BP层
model.add(Activation('sigmoid'))  #添加激活函数
# model.add(Dense(4,input_dim=8,kernel_initializer='uniform')) #输入层，Dense表示BP层
# model.add(Activation('sigmoid'))  #添加激活函数
model.add(Dense(1,input_dim=5))  #输出层
adam = Adam(learning_rate=learning_rate)
model.compile(loss='mean_squared_error', optimizer=adam) #编译模型
model.fit(x_train, y_train, epochs = epochs, batch_size = batch_size,callbacks=[TensorBoard(log_dir='./log')],validation_freq = 0) #训练模型1000次
model.save(modelfile) #保存模型

y = model.predict(x_test) * test_std[label_str] + test_mean[label_str]

y_test = pd.DataFrame(y_test)[0].tolist()
y = pd.DataFrame(y)[0].tolist()

score = mean_squared_error(y_test,y)
score2 = r2_score(y,y_test)

print('RON_均方差：' + str(score))
print('RON_R2：' + str(score2))

# ron_loss_pre = data_test_0['RON'].values - y
# ron_loss = data_test_0['RON损失'].values

# score = mean_squared_error(ron_loss,ron_loss_pre)
# score2 = r2_score(ron_loss,ron_loss_pre)

# print('RON_LOSS_均方差：' + str(score))
# print('RON_LOSS_R2：' + str(score2))

# data_pre = pd.DataFrame({
#     'RON': y_test,
#     'RON_PRED':y})

data_pre_loss = pd.DataFrame({
    'RON_LOSS': y_test,
    'RON_LOSS_PRED':y})

#6 画出预测结果图
import matplotlib.pyplot as plt 
# p = data_pre[['RON','RON_PRED']].plot(style=['b-o','r-*'])
p = data_pre_loss[['RON_LOSS','RON_LOSS_PRED']].plot(style=['b-o','r-*'])
plt.show()