{ pkgs ? import <nixpkgs> {} }:

with pkgs;
let tex-combined = texlive.combine {
      inherit (texlive) scheme-medium collection-latexextra
        collection-bibtexextra collection-publishers;
    };
    rDeps = with rPackages; [ R
                              ggplot2 openxlsx
                              randomForest MASS factoextra pcaMethods
                              neuralnet MLmetrics
                              genalg
                            ];
    pythonPackages = python3Packages;
    pythonDeps = with pythonPackages; [ python numpy ipython pandas
                                        matplotlib # scikit-opt
                                        tensorflow Keras scikitlearn
                                        python3Packages.venvShellHook
                                      ];
in mkShell {
  venvDir = ".venv";
  buildInputs = [ tex-combined ] ++ pythonDeps ++ rDeps;

  postShellHook = ''
    pip install -r requirements.txt
  '';
}
