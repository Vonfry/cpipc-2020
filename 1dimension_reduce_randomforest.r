library(openxlsx)

processed <- read.xlsx("./data/processed.xlsx", rowNames = T)

library(randomForest)

prerf <- processed[, ! colnames(processed) %in% c("时间", "_辛烷值RON", "RON损失（不是变量）")]
# prerf[, "RONL"] <- processed[,"RON损失（不是变量）"]

rf <- randomForest(x = prerf, y = processed[, "RON损失（不是变量）"])
imp <- importance(rf)
ordered_imp <- imp[order(imp, decreasing = T),]

write.csv(ordered_imp, "./data/features_importance.csv")

# chi test
library(MASS)
