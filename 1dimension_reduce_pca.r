library(openxlsx)

processed <- read.xlsx("./data/processed.xlsx", rowNames = T)

prepca <- processed[, ! colnames(processed) %in% c("时间", "_辛烷值RON", "RON损失（不是变量）")]
pca <- prcomp(prepca, scale = TRUE, rank. = 24)

library(factoextra)
jpeg("./output/pca.jpg", width = 1360, height = 1360)
fviz_eig(pca)
dev.off()

jpeg("./output/pca-acc-contrib.jpg", width = 1360, height = 1360)
eig <- get_eig(pca)
eig_part <- eig[1:8,]
ggplot(eig_part,
       aes(x = rownames(eig_part),
           y = cumulative.variance.percent,
           group = 1)) +
    geom_bar(stat="identity",
             fill= "steelblue",
             color = "steelblue"
             ) +
    geom_line(color = "black", linetype = "solid") +
    geom_point(shape = 19, color = "black") +
    labs(title = "Scree plot",
         x = "Dimensions",
         y = "cumulative.variance.percent")
dev.off()

jpeg("./output/pca-ind.jpg", width = 1360, height = 1360)
fviz_pca_ind(pca,
             col.ind = "cos2", # Color by the quality of representation
             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
             repel = TRUE     # Avoid text overlapping
             )
dev.off()

jpeg("./output/pca-biplot.jpg", width = 1360, height = 1360)
fviz_pca_biplot(pca, repel = TRUE,
                col.var = "#2E9FDF", # Variables color
                col.ind = "#696969"  # Individuals color
                )
dev.off()

jpeg("./output/pca-contrib.jpg", width = 1360, height = 1360)
fviz_pca_var(pca,
             col.var = "contrib", # Color by contributions to the PC
             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
             repel = TRUE     # Avoid text overlapping
             )
dev.off()

write.csv(pca$x, "./data/data-after-pca.csv")

# non-linear pca
# library(pcaMethods)
# nlpca <- nlpca(prepca, nPcs = 24, maxSteps = 2 * prod(dim(prepca)))


# use random forest to select features
