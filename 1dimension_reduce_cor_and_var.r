library(openxlsx)

processed <- read.xlsx("./data/processed.xlsx", rowNames = T)

# correlation
cor_processed <- lapply(
    colnames(processed)[3:368],
    function(col) {
        cor(processed["RON损失（不是变量）"],
            processed[col],
            method = c("pearson", "kendall", "spearman"))
    }
)

write.csv(cor_processed, "./data/processed_cor.csv")
write.xlsx(cor_processed, "./data/processed_cor.xlsx")

# Variance
var_processed <- lapply(
    colnames(processed)[3:368],
    function(col) {
        var(processed[col])
    }
)

write.csv(var_processed, "./data/processed_var.csv")
write.xlsx(var_processed, "./data/processed_var.xlsx")
